puppet
======


# Setting up new node

On the new node:

```
$ apt-get install puppet
```

On the puppet master sign the certificate:

```
$ puppet cert list
$ puppet cert sign "foobar.i.ddenis.info"
```

## Generate host keys

```
$ NAME=foobar &&
  ssh-keygen -q -f ssh_host_dsa_key     -N '' -C "hostkey@$NAME" -t dsa             &&
  ssh-keygen -q -f ssh_host_rsa_key     -N '' -C "hostkey@$NAME" -t rsa     -b 4096 &&
  ssh-keygen -q -f ssh_host_ecdsa_key   -N '' -C "hostkey@$NAME" -t ecdsa   -b 521  &&
  ssh-keygen -q -f ssh_host_ed25519_key -N '' -C "hostkey@$NAME" -t ed25519
```

Fill in the blanks in the hiera config file:

```
$ echo "========= Copy these to hieradata/nodes/<name>.yaml:" &&
  echo &&
  for algo in dsa rsa ecdsa ed25519; do
    echo "profile::base::ssh::hostkey::${algo}-public: |"
    cat ssh_host_${algo}_key.pub | sed 's/^/    /'
  done &&
  echo && echo &&
  echo "========== Copy these to hieradata/nodes-secrets/<name>.yaml:" &&
  echo &&
  for algo in dsa rsa ecdsa ed25519; do
    echo "profile::base::ssh::hostkey::${algo}-private: |"
    cat ssh_host_${algo}_key | sed 's/^/    /'
  done &&
  echo && echo &&
  echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!' &&
  echo '!! DO NOT FORGET TO REMOVE THE GENERATE KEYS FROM YOUR SYSTEM !!' &&
  echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
```

# Installing new puppet modules

```
$ puppet module install --target-dir ./modules/ <module-name>
```
