#!/bin/bash

annex_user=annex

if [ "$(whoami)" != "$annex_user" ]; then
    echo "This script should be run as the user 'annex'"
    exit 1
fi

annexes=(
    vir-backups
    vir-media
    vir-private
    vir-photos
    vorlon-documents
)

annex_root=/srv/annex

for annex in ${annexes[@]}; do
    (cd $annex_root/$annex
	echo "======== $(date) syncing $annex"
	git annex sync --no-commit
    echo "======== $(date) DONE")
done
