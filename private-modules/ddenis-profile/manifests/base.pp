class profile::base {
	class {'profile::base::sudo': }
	class {'profile::base::account':
		require => Class['profile::base::sudo'],
	}
	class {'profile::base::packages': }
	class {'profile::base::ntp': }
    class {'profile::base::motd': }
    class {'profile::base::ssh': }
    class {'profile::base::apt': }
}
