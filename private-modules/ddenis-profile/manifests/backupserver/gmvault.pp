class profile::backupserver::gmvault {
    $version = hiera('profile::backupserver::gmvault::version')

    file { '/srv/gmvault':
        ensure  => directory,
        mode    => '0755',
    }
    file { '/srv/gmvault/config':
        ensure  => directory,
        mode    => '0755',
        require => File['/srv/gmvault'],
    }
    file { '/srv/gmvault/data':
        ensure  => directory,
        mode    => '0755',
        require => File['/srv/gmvault'],
    }

    file { '/srv/gmvault/config/cron.d':
        ensure  => directory,
        mode    => '0755',
        require => File['/srv/gmvault/config'],
    }

    file { '/srv/gmvault/config/cron.d/gmvault':
        ensure  => file,
        mode    => '0644',
        source  => 'puppet:///modules/profile/backupserver/gmvault/cronjob.conf',
        require => File['/srv/gmvault/config/cron.d'],
    }

    file { '/srv/gmvault/config/dotgmvault':
        ensure  => directory,
        mode    => '0755',
        require => File['/srv/gmvault/config'],
    }

    file { '/srv/gmvault/config/dotgmvault/gmvault_defaults.conf':
        ensure  => file,
        mode    => '0644',
        source  => 'puppet:///modules/profile/backupserver/gmvault/gmvault_defaults.conf',
        require => File['/srv/gmvault/config/dotgmvault'],
    }

    file { '/srv/gmvault/config/dotgmvault/denis@ddenis.info.oauth2':
        ensure  => file,
        mode    => '0600',
        source  => 'puppet:///modules/profile/backupserver/gmvault/denis@ddenis.info.oauth2',
        require => File['/srv/gmvault/config/dotgmvault'],
    }
    file { '/srv/gmvault/config/dotgmvault/shadone@gmail.com.oauth2':
        ensure  => file,
        mode    => '0600',
        source  => 'puppet:///modules/profile/backupserver/gmvault/shadone@gmail.com.oauth2',
        require => File['/srv/gmvault/config/dotgmvault'],
    }

    docker::image { 'ddenis/gmvault':
        ensure    => present,
        image_tag => $version,
    }
    docker::run { 'gmvault':
        image   => "ddenis/gmvault:${version}",
        command => 'gmvault',
        volumes => [
            '/srv/gmvault/config:/config',
            '/srv/gmvault/data:/data',
        ],
        require => [
            File['/srv/gmvault/config/dotgmvault/shadone@gmail.com.oauth2'],
            File['/srv/gmvault/config/dotgmvault/denis@ddenis.info.oauth2'],
            File['/srv/gmvault/config/dotgmvault/gmvault_defaults.conf'],
            File['/srv/gmvault/config/cron.d/gmvault'],
            File['/srv/gmvault/data'],
        ],
    }
}