class profile::backupserver::coldstorage {
    fstab::mount { '/mnt/media01':
        ensure  => present,
        device  => 'LABEL=media01',
        fstype  => 'ext4',
        options => 'errors=remount-ro,noatime,nodiratime,noauto',
    }
    fstab::mount { '/mnt/media02':
        ensure  => present,
        device  => 'LABEL=media02',
        fstype  => 'ext4',
        options => 'errors=remount-ro,noatime,nodiratime,noauto',
    }
}
