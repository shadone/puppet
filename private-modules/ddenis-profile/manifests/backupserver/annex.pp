class profile::backupserver::annex {
    $extra_ssh_keys = hiera('profile::backupserver::annex::extra-ssh-keys', [])

    ensure_packages([
        'gnupg2',
        'gnupg-agent',
        'gnupg-curl',
        'pcscd',
        'scdaemon',
    ])

    #
    # Install newer version of git-annex
    #

    $neuro_debian_key_id = 'DD95CC430502E37EF840ACEEA5D32F012649A5A9'
    $neuro_debian_key = '-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGiBEQ7TOgRBADvaRsIZ3VZ6Qy7PlDpdMm97m0OfvouOj/HhjOM4M3ECbGn4cYh
vN1gK586s3sUsUcNQ8LuWvNsYhxYsVTZymCReJMEDxod0U6/z/oIbpWv5svF3kpl
ogA66Ju/6cZx62RiCSOkskI6A3Waj6xHyEo8AGOPfzbMoOOQ1TS1u9s2FwCgxziL
wADvKYlDZnWM03QtqIJVD8UEAOks9Q2OqFoqKarj6xTRdOYIBVEp2jhozZUZmLmz
pKL9E4NKGfixqxdVimFcRUGM5h7R2w7ORqXjCzpiPmgdv3jJLWDnmHLmMYRYQc8p
5nqo8mxuO3zJugxBemWoacBDd1MJaH7nK20Hsk9L/jvU/qLxPJotMStTnwO+EpsK
HlihA/9ZpvzR1QWNUd9nSuNR3byJhaXvxqQltsM7tLqAT4qAOJIcMjxr+qESdEbx
NHM5M1Y21ZynrsQw+Fb1WHXNbP79vzOxHoZR0+OXe8uUpkri2d9iOocre3NUdpOO
JHtl6cGGTFILt8tSuOVxMT/+nlo038JQB2jARe4B85O0tkPIPbQybmV1cm8uZGVi
aWFuLm5ldCBhcmNoaXZlIDxtaWNoYWVsLmhhbmtlQGdtYWlsLmNvbT6IRgQQEQgA
BgUCTVHJKwAKCRCNEUVjdcAkyOvzAJ0abJz+f2a6VZG1c9T8NHMTYh1atwCgt0EE
3ZZd/2in64jSzu0miqhXbOKISgQQEQIACgUCSotRlwMFAXgACgkQ93+NsjFEvg8n
JgCfWcdJbILBtpLZCocvOzlLPqJ0Fn0AoI4EpJRxoUnrtzBGUC1MqecU7WsDiGAE
ExECACAFAkqLUWcCGwMGCwkIBwMCBBUCCAMEFgIDAQIeAQIXgAAKCRCl0y8BJkml
qVklAJ4h2V6MdQkSAThF5c2Gkq6eSoIQYQCeM0DWyB9Bl+tTPSTYXwwZi2uoif20
QmFwc3kuZ3NlLnVuaS1tYWdkZWJ1cmcuZGUgRGViaWFuIEFyY2hpdmUgPG1pY2hh
ZWwuaGFua2VAZ21haWwuY29tPohGBBARAgAGBQJEO03FAAoJEPd/jbIxRL4PU18A
n3tn7i4qdlMi8kHbYWFoabsKc9beAJ9sl/leZNCYNMGhz+u6BQgyeLKw94heBBMR
AgAeBQJEO0zoAhsDBgsJCAcDAgMVAgMDFgIBAh4BAheAAAoJEKXTLwEmSaWpVdoA
n27DvtZizNEbhz3wRUPQMiQjtqdvAJ9rS9YdPe5h5o5gHx3mw3BSkOttdYheBBMR
AgAeBQJEO0zoAhsDBgsJCAcDAgMVAgMDFgIBAh4BAheAAAoJEKXTLwEmSaWpVdoA
oLhwWL+E+2I9lrUf4Lf26quOK9vLAKC9ZpIF2tUirFFkBWnQvu13/TA0SokCHAQQ
AQIABgUCTSNBgQAKCRDAc9Iof/uem4NpEACQ8jxmaCaS/qk/Y4GiwLA5bvKosG3B
iARZ2v5UWqCZQ1tS56yKse/lCIzXQqU9BnYW6wOI2rvFf9meLfd8h96peG6oKscs
fbclLDIf68bBvGBQaD0VYFi/Fk/rxmTQBOCQ3AJZs8O5rIM4gPGE0QGvSZ1h7VRw
3Uyeg4jKXLIeJn2xEmOJgt3auAR2FyKbzHaX9JCoByJZ/eU23akNl9hgt7ePlpXo
74KNYC58auuMUhCq3BQDB+II4ERYMcmFp1N5ZG05Cl6jcaRRHDXz+Ax6DWprRI1+
RH/Yyae6LmKpeJNwd+vM14aawnNO9h8IAQ+aJ3oYZdRhGyybbin3giJ10hmWveg/
Pey91Nh9vBCHdDkdPU0s9zE7z/PHT0c5ccZRukxfZfkrlWQ5iqu3V064ku5f4PBy
8UPSkETcjYgDnrdnwqIAO+oVg/SFlfsOzftnwUrvwIcZlXAgtP6MEEAs/38e/JIN
g4VrpdAy7HMGEUsh6Ah6lvGQr+zBnG44XwKfl7e0uCYkrAzUJRGM5vx9iXvFMcMu
jv9EBNNBOU8/Y6MBDzGZhgaoeI27nrUvaveJXjAiDKAQWBLjtQjINZ8I9uaSGOul
8kpbFavE4eS3+KhISrSHe4DuAa3dk9zI+FiPvXY1ZyfQBtNpR+gYFY6VxMbHhY1U
lSLHO2eUIQLdYbRITmV1cm9EZWJpYW4gQXJjaGl2ZSBLZXkgPHBrZy1leHBwc3kt
bWFpbnRhaW5lcnNAbGlzdHMuYWxpb3RoLmRlYmlhbi5vcmc+iEYEEBEIAAYFAk1R
yQYACgkQjRFFY3XAJMgEWwCggx4Gqlcrt76TSMlbU94cESo55AEAoJ3asQEMpe8t
QUX+5aikw3z1AUoCiEoEEBECAAoFAkqf/3cDBQF4AAoJEPd/jbIxRL4PxyMAoKUI
RPWlHCj/+HSFfwhos68wcSwmAKChuC00qutDro+AOo+uuq6YoHXj+ohgBBMRAgAg
BQJKn/8bAhsDBgsJCAcDAgQVAggDBBYCAwECHgECF4AACgkQpdMvASZJpalDggCe
KF9KOgOPdQbFnKXl8KtHory4EEwAnA7jxgorE6kk2QHEXFSF8LzOOH4GiGMEExEC
ACMCGwMGCwkIBwMCBBUCCAMEFgIDAQIeAQIXgAUCSp//RgIZAQAKCRCl0y8BJkml
qekFAKCRyt4+FoCzmBbRUUP3Cr8PzH++IgCgkno4vdjsWdyAey8e0KpITTXMFrmJ
AhwEEAECAAYFAk0jQYEACgkQwHPSKH/7npsFfw/+P8B8hpM3+T1fgboBa4R32deu
n8m6b8vZMXwuo/awQtMpzjem8JGXSUQm8iiX4hDtjq6ZoPrlN8T4jNmviBt/F5jI
Jji/PYmhq+Zn9s++mfx+aF4IJrcHJWFkg/6kJzn4oSdl/YlvKf4VRCcQNtj4xV87
GsdamnzU17XapLVMbSaVKh+6Af7ZLDerEH+iAq733HsYaTK+1xKmN7EFVXgS7bZ1
9C4LTzc97bVHSywpT9yIrg9QQs/1kshfVIHDKyhjF6IwzSVbeGAIL3Oqo5zOMkWv
7JlEIkkhTyl+FETxNMTMYjAk+Uei3kRodneq3YBF2uFYSEzrXQgHAyn37geiaMYj
h8wu6a85nG1NS0SdxiZDIePmbvD9vWxFZUWYJ/h9ifsLivWcVXlvHoQ0emd+n2ai
FhAck2xsuyHgnGIZMHww5IkQdu/TMqvbcR6d8Xulh+C4Tq7ppy+oTLADSBKII++p
JQioYydRD529EUJgVlhyH27X6YAk3FuRD3zYZRYS2QECiKXvS665o3JRJ0ZSqNgv
YOom8M0zz6bI9grnUoivMI4o7ISpE4ZwffEd37HVzmraaUHDXRhkulFSf1ImtXoj
V9nNSM5p/+9eP7OioTZhSote6Vj6Ja1SZeRkXZK7BwqPbdO0VsYOb7G//ZiOlqs+
paRr92G/pwBfj5Dq8EK5Ag0ERDtM9RAIAN0EJqBPvLN0tEin/y4Fe0R4n+E+zNXg
bBsq4WidwyUFy3h/6u86FYvegXwUqVS2OsEs5MwPcCVJOfaEthF7I89QJnP9Nfx7
V5I9yFB53o9ii38BN7X+9gSjpfwXOvf/wIDfggxX8/wRFel37GRB7TiiABRArBez
s5x+zTXvT++WPhElySj0uY8bjVR6tso+d65K0UesvAa7PPWeRS+3nhqABSFLuTTT
MMbnVXCGesBrYHlFVXClAYrSIOX8Ub/UnuEYs9+hIV7U4jKzRF9WJhIC1cXHPmOh
vleAf/I9h/0KahD7HLYud40pNBo5tW8jSfp2/Q8TIE0xxshd51/xy4MAAwUH+wWn
zsYVk981OKUEXul8JPyPxbw05fOd6gF4MJ3YodO+6dfoyIl3bewk+11KXZQALKaO
1xmkAEO1RqizPeetoadBVkQBp5xPudsVElUTOX0pTYhkUd3iBilsCYKK1/KQ9KzD
I+O/lRsm6L9lc6rV0IgPU00P4BAwR+x8Rw7TJFbuS0miR3lP1NSguz+/kpjxzmGP
LyHJ+LVDYFkk6t0jPXhqFdUY6McUTBDEvavTGlVO062l9APTmmSMVFDsPN/rBes2
rYhuuT+lDp+gcaS1UoaYCIm9kKOteQBnowX9V74Z+HKEYLtwILaSnNe6/fNSTvyj
g0z+R+sPCY4nHewbVC+ISQQYEQIACQUCRDtM9QIbDAAKCRCl0y8BJkmlqbecAJ9B
UdSKVg9H+fQNyP5sbOjj4RDtdACfXHrRHa2+XjJP0dhpvJ8IfvYnQsU=
=fAJZ
-----END PGP PUBLIC KEY BLOCK-----
'
    apt::source { 'NeuroDebianXenialLibre':
        comment  => 'NeuroDebian team http://neuro.debian.net',
        location => 'http://neurodebian.g-node.org',
        release  => 'xenial',
        repos    => 'main',
        key      => {
            id      => $neuro_debian_key_id,
            content => $neuro_debian_key,
        },
        include  => {
            'src' => false,
            'deb' => true,
        },
        notify   => Exec['apt_update'],
    }

    apt::source { 'NeuroDebianDataLibre':
        comment  => 'NeuroDebian team http://neuro.debian.net',
        location => 'http://neurodebian.g-node.org',
        release  => 'data',
        repos    => 'main',
        key      => {
            id      => $neuro_debian_key_id,
            content => $neuro_debian_key,
        },
        include  => {
            'src' => false,
            'deb' => true,
        },
        notify   => Exec['apt_update'],
    }

    package { 'git-annex-standalone':
        ensure => installed,
        require => [
            Apt::Source['NeuroDebianXenialLibre'],
            Apt::Source['NeuroDebianDataLibre'],
        ],
    }

    #
    # Create dedicated user
    #

    user { 'annex':
        ensure     => present,
        shell      => '/bin/bash',
        comment    => 'git-annex user',
        home       => '/home/annex',
        groups     => ['ssh-sure-thing'],
        password   => '!!',
    }

    group { 'annex':
        ensure => present,
    }

    file { '/home/annex':
        ensure  => directory,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0755',
        require => [
            User['annex'],
            Group['annex'],
        ],
    }

    file { '/home/annex/.ssh':
        ensure  => directory,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0700',
        require => [
            File['/home/annex'],
        ],
    }

    file { '/home/annex/.ssh/config':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0644',
        source  => 'puppet:///modules/profile/backupserver/annex/ssh_config',
        require => [
            File['/home/annex/.ssh'],
        ],
    }

    file { '/home/annex/.ssh/git-annex-shell':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0755',
        source  => 'puppet:///modules/profile/backupserver/git-annex-shell',
        require => [
            File['/home/annex/.ssh'],
        ],
    }

    file { '/home/annex/.ssh/id_rsa':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0600',
        source  => 'puppet:///modules/profile/backupserver/annex/id_rsa',
        require => [
            File['/home/annex/.ssh'],
        ],
    }

    file { '/home/annex/.ssh/id_rsa.pub':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0600',
        source  => 'puppet:///modules/profile/backupserver/annex/id_rsa.pub',
        require => [
            File['/home/annex/.ssh'],
        ],
    }

    ssh_authorized_key { 'annex-ssh-authorized-key-git-annex@vir':
        ensure  => present,
        user    => 'annex',
        name    => 'git-annex@vir@annex',
        key     => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDKBjpgyXcPWzon2uzQz1kUlDtO+dvdqWkfMIo/xNyx8FQamzRiaH0VX/6gnq3qr08MFH+FSp6FvN55kAuQmx+bi8e/e6v7VoGprpgORX5wPNpqWwWlOv8c/pIJEaI1fk0od8AuzTKf35o82Jqq9ohl0cNaGy7aNAvPS1tVNJ038FM0VpoaUx0WXxBHs1Xgup2KSoaWhNffhURiHMn1q7ynnFOPNysGhA6kQENHOhrKKywTR2WZX1CGQuZheAQLNQwIBVd+/PQMxyzZbrEDYdSsmn6d7d53d7pVP2OKaBjO4IzC0Ahl5QwGJSxZu5Ytx6hWLUEuWuugNo0KhNS7ca65ISShORyTgMwWEmM3wx1x+Dsey9g+HFlk7ull7al+XOODu7JT+7UTvg2fr4ai+tdYkZ6Zmw+gu8wKjM4OA0pOk2SQlbvoJKiQx+ocW+OwEt9SINfMSEQQGA5dR82iTBDr5VIwen0pUW2UP9R3Y39FKW3q6ocvYdvJBVpiv0R+sO/JvavAJbDJWpSpefWBRJXJ3ZL3kpyqkmbyShkRiLFHauaz+l18HgNQ4q0yi3wW8hgntEBGk73lErDMKALlVswMFaLCVfT5MsIRGdd3ADHlMDQIhDvOi1TVfslxdr3b+ibOBERtV/K6ukvQ3fiZcdVD0DLdYL4ENWXc2dXRObKQ9Q==',
        type    => 'ssh-rsa',
        options => 'command="/home/annex/.ssh/git-annex-shell"',
        require => [
            File['/home/annex/.ssh'],
        ],
    }

    ssh_authorized_key { 'annex-ssh-authorized-key-git-annex@minimac':
        ensure  => present,
        user    => 'annex',
        name    => 'git-annex@minimac@annex',
        key     => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQCqUIYru6vIuCHCMSLR0JkqztZDsvsIoVSpyVGtgJALocnz4hBhaJvw5nLPIrff4P9cPV7/0L83Kzk+wqFk8O1H49V5TkPhHgn5HPHdDsh9MzKTOl9BzEdHhY9zLlpdxT1y+0KJZeSEirMnidLvQVcQ479b/J0iu1Z9Fje/5CiEHLDdnSO3df4b7nLqMtOUmkDjA2YAgbbivAW4x6MIRxnJm6pxF7smwFDpct6Jlwt/qGbyjdMazcjtidpubwLx15FhiVZovzpT7oZD5iYlS2biy65Y5IyYgv9dEjRWMZZ8Dcc0jjOkUm6HCLyVvml02Cy6MKqltRhz4dyctv8hFL0iHyGBXrOpombL1r2fmn6C9zMa24hzH9iuB+eiSWiBe3KcDSiJDx/etBhNbJRA7E0GXWrUugPUOjOIMmVEGmAClbiDLv4lYFoYZMzwkemVgCCTnaccfF4OCeKEvWzmJ/fIXvcXtf/hqy8Vtl8Z8FSIMCsj1nJQBceFs063e4WrQibf42ffDzK2cC0UT1P4LrnvDQvJOCGSqzjKtvBzRxaVy7nhhY7kMCuNbWlf3uTXQwJr4g/+HLh/7vCP1C63fsm8bS78mUYtQfGpk+9yumWuSdCom9IIq8C4NFmgblNhMbGSpcRVo0//TXcMq2/C4Myt8/HO+QqtQow75JzsRM0/yw==',
        type    => 'ssh-rsa',
        options => 'command="/home/annex/.ssh/git-annex-shell"',
        require => [
            File['/home/annex/.ssh'],
        ],
    }

    ssh_authorized_key { 'annex-ssh-authorized-key-git-annex@vorlon':
        ensure  => present,
        user    => 'annex',
        name    => 'git-annex@vorlon@annex',
        key     => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDbMdLJE8YaCE/vmm79gTZ0jhdX9J9P7n1rl4oYfRlZfUbB8lSBdde/QveYCspvX7eb7yHhbdn8MzLoR4I2RLLxyfg8y3KMaqdYtQNQHc78OnU7/lzh644hMku1eK1yHtyUkpzK+LXs5EHLU6JWh6FzNCwk6YvM+VgXIXQZpnaLUwXwlpbscINzxrL5cWDjki8J6zvUzZ6OSD1e4su3eabwcya5yOiRa4Tl9uky/VMPrZ3/UCtV2MQ5630sZ6Wlxavr2X0xP1N7Sfr+nIk92p2Ls1cER+prcEsdPcwC9HIPmCfMN0c6zwMb/T0RQbDTWrhTDbUy+fpBbcko1rABqehB7zICYa4yxtmGkTE2mRKmGvTpGeu8NwTZpGEXayu8P6I53CcttmevZ1ASCJ+kwiwjQfbf1luF+R1iQBek58jmY7czg8eCo+hvkOKNQKqs6iMTKf0SzRVRZM9lz1qdEBnV/UWJxJ8G2FaW1uDsWonIZHTP2D7qYx/iXyPV5kumhxAb/eG0PF4237Ml/jbkhE7lZ8nejN60E98/X2oNZ+zo9ALOjT90RWI0yOR7d0oKUooso9WWgfYbIOdDvwvsuoqI26CzrbAzzbtc6BuXVP8sIX/NUGRIEyrll4VNc1dNrZVv7LLZg8KT/7tvp+orb5yzCLJHHCYW0KioW2fT3zdelQ==',
        type    => 'ssh-rsa',
        options => 'command="/home/annex/.ssh/git-annex-shell"',
        require => [
            File['/home/annex/.ssh'],
        ],
    }

    create_resources('ssh_authorized_key', $extra_ssh_keys, {
        user    => 'annex',
        require => File['/home/annex/.ssh'],
    })

    file { '/srv/annex':
        ensure  => directory,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0755',
        require => [
            User['annex'],
            Group['annex'],
        ],
    }

    exec { 'annex-known-hosts-vir':
        command => 'echo "|1|XKQt0Ns7Rj+wsoDorS/tSCoJhLM=|JT3g1e8WnH9a3+HaZDledKpDBqA= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJsDqrsYCaJjZPZo3qDJODXfShFLdWJZpe4fMhWuBhHyAMkdj+K4qlzvOjF7DGNTPoTCzgeC1ZTYo7/Ydo9TeO4=\n|1|4hCHYT9eQwQkqLHaAcqjZya/mZo=|IXFLXKMbjxFAsaCZAJmKnDY54wc= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJsDqrsYCaJjZPZo3qDJODXfShFLdWJZpe4fMhWuBhHyAMkdj+K4qlzvOjF7DGNTPoTCzgeC1ZTYo7/Ydo9TeO4=" >> /home/annex/.ssh/known_hosts',
        unless  => 'grep \'|1|XKQt0Ns7Rj+wsoDorS/tSCoJhLM=|JT3g1e8WnH9a3+HaZDledKpDBqA= \' /home/annex/.ssh/known_hosts 2>/dev/null',
        cwd     => '/home/annex',
        path    => ['/sbin', '/usr/sbin', '/bin', '/usr/bin'],
        user    => 'annex',
        group   => 'annex',
        timeout => 600,
        require => [
            File['/home/annex/.ssh'],
        ],
    }
    exec { 'annex-known-hosts-minimac':
        command => 'echo "|1|taqFDJeW6sEmEuhw6Xnojbtpfoo=|rXwWG3ubLcCuHpFKGkHzn9X22rM= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAsjohwzVzYHJBePmL6ustm7WOyV0R/E/SqwbA0zZzR46I3byxTrN0xS+i6aJOlikMd+HZEMGBQITx/8QaX98jk=\n|1|tzNoDijY8DvELmj4kdCFSDuhBq0=|k5ADdyID8nlvRzE96cMtl0OA80s= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAsjohwzVzYHJBePmL6ustm7WOyV0R/E/SqwbA0zZzR46I3byxTrN0xS+i6aJOlikMd+HZEMGBQITx/8QaX98jk=" >> /home/annex/.ssh/known_hosts',
        unless  => 'grep \'|1|taqFDJeW6sEmEuhw6Xnojbtpfoo=|rXwWG3ubLcCuHpFKGkHzn9X22rM= \' /home/annex/.ssh/known_hosts 2>/dev/null',
        cwd     => '/home/annex',
        path    => ['/sbin', '/usr/sbin', '/bin', '/usr/bin'],
        user    => 'annex',
        group   => 'annex',
        timeout => 600,
        require => [
            File['/home/annex/.ssh'],
        ],
    }
    exec { 'annex-known-hosts-drazi':
        command => 'echo "|1|x5pY2lbzR8L7dffiIRyDhBC+D3c=|jVO/zr8mkCT08Oa7ttAdpTl7OZ8= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBI0peimBH1kbQq2r1xTaqlsc/m5mfha0vFydvBtf+TeOSLBaShzpJygtW5lEZQJb/23DR5oPkXW47VuV3UYzkmQ=\n|1|FsEdHHKjz6+TiMqYItg4R+Rxba4=|lSl4wBX4bK2WlKtCVrFrxHHBmWY= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBI0peimBH1kbQq2r1xTaqlsc/m5mfha0vFydvBtf+TeOSLBaShzpJygtW5lEZQJb/23DR5oPkXW47VuV3UYzkmQ=" >> /home/annex/.ssh/known_hosts',
        unless  => 'grep \'|1|x5pY2lbzR8L7dffiIRyDhBC+D3c=|jVO/zr8mkCT08Oa7ttAdpTl7OZ8= \' /home/annex/.ssh/known_hosts 2>/dev/null',
        cwd     => '/home/annex',
        path    => ['/sbin', '/usr/sbin', '/bin', '/usr/bin'],
        user    => 'annex',
        group   => 'annex',
        timeout => 600,
        require => [
            File['/home/annex/.ssh'],
        ],
    }
    exec { 'annex-known-hosts-backup.drazi':
        command => 'echo "|1|HPusI9w6WcmFbYx8fjaVJNlScek=|uaPufTOikhrAN5fiei0JZrKV5MI= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBG4zUW2Zc7L3GAkBwWJKkkpsQmJQ/55y7JMxrpqeCZ5rFPgdzQXM378HZ4Au8sHLeDIQIHImA14XNccUpjG0ojU=" >> /home/annex/.ssh/known_hosts',
        unless  => 'grep \'|1|HPusI9w6WcmFbYx8fjaVJNlScek=|uaPufTOikhrAN5fiei0JZrKV5MI= \' /home/annex/.ssh/known_hosts 2>/dev/null',
        cwd     => '/home/annex',
        path    => ['/sbin', '/usr/sbin', '/bin', '/usr/bin'],
        user    => 'annex',
        group   => 'annex',
        timeout => 600,
        require => [
            File['/home/annex/.ssh'],
        ],
    }

    #
    # Maintenance
    #

    file { '/srv/annex/sync-all.sh':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0755',
        source  => 'puppet:///modules/profile/backupserver/annex/sync-all.sh',
        require => [
            File['/srv/annex'],
        ],
    }

    file { '/var/log/annex':
        ensure  => directory,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0755',
        require => [
            User['annex'],
            Group['annex'],
        ],
    }

    file { '/etc/logrotate.d/annex-sync-all':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet:///modules/profile/backupserver/annex/sync-all-logrotate.conf',
    }

    file { '/etc/cron.d/annex':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => '40 23 * * * annex /srv/annex/sync-all.sh 2>&1 >>/var/log/annex/sync-all.log',
        require => [
            File['/srv/annex/sync-all.sh'],
            File['/var/log/annex'],
        ],
    }

    #
    # gnupg
    #

    file { '/home/annex/.gnupg':
        ensure  => directory,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0700',
        require => User['annex'],
    }

    file { '/home/annex/.gnupg/sks-keyservers.netCA.pem':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0644',
        content => '-----BEGIN CERTIFICATE-----
MIIFizCCA3OgAwIBAgIJAK9zyLTPn4CPMA0GCSqGSIb3DQEBBQUAMFwxCzAJBgNV
BAYTAk5PMQ0wCwYDVQQIDARPc2xvMR4wHAYDVQQKDBVza3Mta2V5c2VydmVycy5u
ZXQgQ0ExHjAcBgNVBAMMFXNrcy1rZXlzZXJ2ZXJzLm5ldCBDQTAeFw0xMjEwMDkw
MDMzMzdaFw0yMjEwMDcwMDMzMzdaMFwxCzAJBgNVBAYTAk5PMQ0wCwYDVQQIDARP
c2xvMR4wHAYDVQQKDBVza3Mta2V5c2VydmVycy5uZXQgQ0ExHjAcBgNVBAMMFXNr
cy1rZXlzZXJ2ZXJzLm5ldCBDQTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoC
ggIBANdsWy4PXWNUCkS3L//nrd0GqN3dVwoBGZ6w94Tw2jPDPifegwxQozFXkG6I
6A4TK1CJLXPvfz0UP0aBYyPmTNadDinaB9T4jIwd4rnxl+59GiEmqkN3IfPsv5Jj
MkKUmJnvOT0DEVlEaO1UZIwx5WpfprB3mR81/qm4XkAgmYrmgnLXd/pJDAMk7y1F
45b5zWofiD5l677lplcIPRbFhpJ6kDTODXh/XEdtF71EAeaOdEGOvyGDmCO0GWqS
FDkMMPTlieLA/0rgFTcz4xwUYj/cD5e0ZBuSkYsYFAU3hd1cGfBue0cPZaQH2HYx
Qk4zXD8S3F4690fRhr+tki5gyG6JDR67aKp3BIGLqm7f45WkX1hYp+YXywmEziM4
aSbGYhx8hoFGfq9UcfPEvp2aoc8u5sdqjDslhyUzM1v3m3ZGbhwEOnVjljY6JJLx
MxagxnZZSAY424ZZ3t71E/Mn27dm2w+xFRuoy8JEjv1d+BT3eChM5KaNwrj0IO/y
u8kFIgWYA1vZ/15qMT+tyJTfyrNVV/7Df7TNeWyNqjJ5rBmt0M6NpHG7CrUSkBy9
p8JhimgjP5r0FlEkgg+lyD+V79H98gQfVgP3pbJICz0SpBQf2F/2tyS4rLm+49rP
fcOajiXEuyhpcmzgusAj/1FjrtlynH1r9mnNaX4e+rLWzvU5AgMBAAGjUDBOMB0G
A1UdDgQWBBTkwyoJFGfYTVISTpM8E+igjdq28zAfBgNVHSMEGDAWgBTkwyoJFGfY
TVISTpM8E+igjdq28zAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4ICAQAR
OXnYwu3g1ZjHyley3fZI5aLPsaE17cOImVTehC8DcIphm2HOMR/hYTTL+V0G4P+u
gH+6xeRLKSHMHZTtSBIa6GDL03434y9CBuwGvAFCMU2GV8w92/Z7apkAhdLToZA/
X/iWP2jeaVJhxgEcH8uPrnSlqoPBcKC9PrgUzQYfSZJkLmB+3jEa3HKruy1abJP5
gAdQvwvcPpvYRnIzUc9fZODsVmlHVFBCl2dlu/iHh2h4GmL4Da2rRkUMlbVTdioB
UYIvMycdOkpH5wJftzw7cpjsudGas0PARDXCFfGyKhwBRFY7Xp7lbjtU5Rz0Gc04
lPrhDf0pFE98Aw4jJRpFeWMjpXUEaG1cq7D641RpgcMfPFvOHY47rvDTS7XJOaUT
BwRjmDt896s6vMDcaG/uXJbQjuzmmx3W2Idyh3s5SI0GTHb0IwMKYb4eBUIpQOnB
cE77VnCYqKvN1NVYAqhWjXbY7XasZvszCRcOG+W3FqNaHOK/n/0ueb0uijdLan+U
f4p1bjbAox8eAOQS/8a3bzkJzdyBNUKGx1BIK2IBL9bn/HravSDOiNRSnZ/R3l9G
ZauX0tu7IIDlRCILXSyeazu0aj/vdT3YFQXPcvt5Fkf5wiNTo53f72/jYEJd6qph
WrpoKqrwGwTpRUCMhYIUt65hsTxCiJJ5nKe39h46sg==
-----END CERTIFICATE-----',
        require => File['/home/annex/.gnupg'],
    }

    file { '/home/annex/.gnupg/gpg.conf':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0644',
        content => '
#-----------------------------
# algorithm and ciphers
#-----------------------------

personal-cipher-preferences AES256 AES192 AES CAST5
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
cert-digest-algo SHA512
s2k-digest-algo SHA512


#-----------------------------
# keyserver
#-----------------------------

auto-key-locate keyserver
keyserver hkps://hkps.pool.sks-keyservers.net
keyserver-options ca-cert-file=/home/annex/.gnupg/sks-keyservers.netCA.pem

# When using --refresh-keys, if the key in question has a preferred keyserver
# URL, then disable use of that preferred keyserver to refresh the key from
keyserver-options no-honor-keyserver-url

# When searching for a key with --search-keys, include keys that are marked on
# the keyserver as revoked
keyserver-options include-revoked


#-----------------------------
# behavior
#-----------------------------

# Display long key IDs
keyid-format 0xlong

# List all keys (or the specified ones) along with their fingerprints
with-fingerprint

# Disable inclusion of the version string in ASCII armored output
no-emit-version

# Disable comment string in clear text signatures and ASCII armored messages
no-comments

# Display the calculated validity of user IDs during key listings
list-options show-uid-validity
verify-options show-uid-validity

# Try to use the GnuPG-Agent. With this option, GnuPG first tries to connect to
# the agent before it asks for a passphrase.
use-agent
',
        require => File['/home/annex/.gnupg'],
    }

    file { '/home/annex/.gnupg/dirmngr.conf':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0644',
        content => 'hkp-cacert /home/annex/.gnupg/sks-keyservers.netCA.pem',
        require => File['/home/annex/.gnupg'],
    }

    file { '/home/annex/.gnupg/gpg-agent.conf':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0644',
        content => 'use-standard-socket
default-cache-ttl 43200
max-cache-ttl 43200
    ',
        require => File['/home/annex/.gnupg'],
    }

    # TODO: refresh gpg keys over Tor using "parcimonie" script
    file { '/etc/cron.d/annex-gpg-refresh-keys': # weekly refresh gpg keys
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => '12 3 * * 0 annex /usr/bin/gpg2 --refresh-keys',
    }

    file { '/home/annex/.bash_profile':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0644',
        content => '# bash config file
# without this gpg-agent launched implicitly by gpg from within git-annex
# process tree will not be able to access pinentry and therefor secret keys
export GPG_TTY=$(tty)

export PATH=$HOME/bin:$PATH
',
        require => File['/home/annex']
    }

    file { '/home/annex/bin':
        ensure  => directory,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0755',
        require => File['/home/annex'],
    }

    file { '/home/annex/bin/gpg':
        ensure  => file,
        owner   => 'annex',
        group   => 'annex',
        mode    => '0755',
        content => '#!/bin/sh
exec /usr/bin/gpg2 $*
',
        require => File['/home/annex/bin']
    }
}
