class profile::base::ssh {
    $hostkey_rsa_public      = hiera('profile::base::ssh::hostkey::rsa-public',      '')
    $hostkey_rsa_private     = hiera('profile::base::ssh::hostkey::rsa-private',     '')
    $hostkey_dsa_public      = hiera('profile::base::ssh::hostkey::dsa-public',      '')
    $hostkey_dsa_private     = hiera('profile::base::ssh::hostkey::dsa-private',     '')
    $hostkey_ecdsa_public    = hiera('profile::base::ssh::hostkey::ecdsa-public',    '')
    $hostkey_ecdsa_private   = hiera('profile::base::ssh::hostkey::ecdsa-private',   '')
    $hostkey_ed25519_public  = hiera('profile::base::ssh::hostkey::ed25519-public',  '')
    $hostkey_ed25519_private = hiera('profile::base::ssh::hostkey::ed25519-private', '')

    class { 'ssh::server':
        storeconfigs_enabled => false,
        validate_sshd_file   => true,
        options => {
            'Protocol'               => 2,
            'PasswordAuthentication' => 'no',
            'ChallengeResponseAuthentication' => 'no',
            'PubkeyAuthentication'   => 'yes',
            'PermitRootLogin'        => 'no',
            'PermitEmptyPasswords'   => 'no',
            'Port'                   => 22,
            'X11Forwarding'          => 'no',
            'PrintMotd'              => 'no',
            'UsePrivilegeSeparation' => 'sandbox',
            'StrictModes'            => 'yes',
            'LoginGraceTime'         => 120,
            'PrintLastLog'           => 'yes',
            'TCPKeepAlive'           => 'yes',
            'MaxStartups'            => '10:30:60',
            'UsePAM'                 => 'yes',
            'AllowGroups'            => 'ssh-sure-thing',
            'Subsystem'              => 'sftp /usr/lib/openssh/sftp-server',
        },
    }

    if (is_string($hostkey_rsa_private) and !empty($hostkey_rsa_private) and
        is_string($hostkey_rsa_public) and !empty($hostkey_rsa_public)) {
        ssh::server::host_key {'ssh_host_rsa_key':
            private_key_content => $hostkey_rsa_private,
            public_key_content  => $hostkey_rsa_public,
        }
    }
    if (is_string($hostkey_dsa_private) and !empty($hostkey_dsa_private) and
        is_string($hostkey_dsa_public) and !empty($hostkey_dsa_public)) {
        ssh::server::host_key {'ssh_host_dsa_key':
            private_key_content => $hostkey_dsa_private,
            public_key_content  => $hostkey_dsa_public,
        }
    }
    if (is_string($hostkey_ecdsa_private) and !empty($hostkey_ecdsa_private) and
        is_string($hostkey_ecdsa_public) and !empty($hostkey_ecdsa_public)) {
        ssh::server::host_key {'ssh_host_ecdsa_key':
            private_key_content => $hostkey_ecdsa_private,
            public_key_content  => $hostkey_ecdsa_public,
        }
    }
    if (is_string($hostkey_ed25519_private) and !empty($hostkey_ed25519_private) and
        is_string($hostkey_ed25519_public) and !empty($hostkey_ed25519_public)) {
        ssh::server::host_key {'ssh_host_ed25519_key':
            private_key_content => $hostkey_ed25519_private,
            public_key_content  => $hostkey_ed25519_public,
        }
    }
}
