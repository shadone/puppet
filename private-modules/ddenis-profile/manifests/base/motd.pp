class profile::base::motd {
    $text = hiera('profile::base::motd::text', '')

    if !empty($text) {
        case $::osfamily {
            'Debian': {
                if $::operatingsystem == 'Ubuntu' {
                    file {'/etc/update-motd.d/50-text':
                        ensure  => file,
                        owner   => 'root',
                        group   => 'root',
                        mode    => '0755',
                        content => "#!/bin/sh\ncat<<EOF\n\n${text}\nEOF\n",
                    }
                } else {
                }
            }
        }
    }
}
