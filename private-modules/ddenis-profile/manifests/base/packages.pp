class profile::base::packages {
    $packages_enabled = hiera('profile::base::packages::enabled', true)
    if $packages_enabled {
        ensure_packages([
            'screen', 'zsh', 'make', 'vim', 'git',
            'whois', 'netcat', 'telnet',
            'ca-certificates', 'smartmontools',
            'curl', 'wget', 'rsync',
            'iftop', 'ifstat',
            'parallel',
            'sysstat',
            'jq',
            'gnupg2', 'gnupg-agent', 'gnupg-curl',
        ])
    }
}
