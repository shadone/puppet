class profile::base::sudo {
    case $::osfamily {
        'Debian': {
            $default_group = 'sudo'
        }
        default: {
            fail('Unhandled osfamily.')
        }
    }

    $group = hiera('profile::base::sudo::group', $default_group)

    if (!is_string($group) or empty($group)) {
        fail('sudo group should be set.')
    }

    include ::sudo

    group { $group: ensure => present }

    ::sudo::conf { 'defaults-mail_badpass':
        priority => 10,
        content  => "Defaults mail_badpass",
    }

    ::sudo::conf { 'sudo-group-no-passwd':
        priority => 20,
        content  => "%${group} ALL=(ALL:ALL) NOPASSWD: ALL",
    }
}
