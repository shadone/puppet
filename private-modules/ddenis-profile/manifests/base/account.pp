class profile::base::account {
	$uid_param = hiera('profile::base::account::uid', undef)
	$shell = hiera('profile::base::account::shell', '/bin/bash')
	$sudo_group = $::profile::base::sudo::group

    if $uid_param == undef or (is_string($uid_param) and empty($uid_param)) {
        $uid = undef
    } elsif is_integer($uid_param) {
        $uid = $uid_param
    } else {
        fail('uid should be a number.')
    }

    group { 'ssh-sure-thing':
        # Only members of this group can ssh
        ensure => present,
    }

    accounts::user { 'denis':
        comment  => 'Denis Dzyubenko',
        uid      => $uid,
        groups   => [ $sudo_group, 'ssh-sure-thing' ],
        shell    => $shell,
        password => '!!',
        sshkeys  => ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGqPDC5KlEpxX1MA/guicQmcMKwgkDSg6fYT7TxNAfH1p9DTYdFeZLYxAISXpUrS9DleQp7/1TeWBAeLndLgl0IEhLySaWrQqDFYsiahKkRg9asypsBQ4iP9CgDKnx3JIYCWczWu5ZwocQxmEIzz+U9SVvzbQBByzHwIJLik7WxWHuiZ9myQ/agaH3kzGoCjUpdeFr0c27j/dQ2G6QRk4L9Vahv7N7kOr/W66/Zx2WJ4ifKqfewKgok1ImHGEyGXZnG1oRx2pEUC1ZhjDr3O41xI5IwdPTo74o5eWJL8zx/xhNZsm31RvhEeDoKlcvhwswPkUsGSAI+h1HNprdYrbt denis@vorlon.local"],
        locked   => false,
        require  => Group['ssh-sure-thing'],
    }

    file { '/home/denis/.gnupg':
        ensure  => directory,
        owner   => 'denis',
        group   => 'denis',
        mode    => '0700',
        require => Accounts::User['denis'],
    }

    file { '/home/denis/.gnupg/sks-keyservers.netCA.pem':
        ensure  => file,
        owner   => 'denis',
        group   => 'denis',
        mode    => '0644',
        content => '-----BEGIN CERTIFICATE-----
MIIFizCCA3OgAwIBAgIJAK9zyLTPn4CPMA0GCSqGSIb3DQEBBQUAMFwxCzAJBgNV
BAYTAk5PMQ0wCwYDVQQIDARPc2xvMR4wHAYDVQQKDBVza3Mta2V5c2VydmVycy5u
ZXQgQ0ExHjAcBgNVBAMMFXNrcy1rZXlzZXJ2ZXJzLm5ldCBDQTAeFw0xMjEwMDkw
MDMzMzdaFw0yMjEwMDcwMDMzMzdaMFwxCzAJBgNVBAYTAk5PMQ0wCwYDVQQIDARP
c2xvMR4wHAYDVQQKDBVza3Mta2V5c2VydmVycy5uZXQgQ0ExHjAcBgNVBAMMFXNr
cy1rZXlzZXJ2ZXJzLm5ldCBDQTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoC
ggIBANdsWy4PXWNUCkS3L//nrd0GqN3dVwoBGZ6w94Tw2jPDPifegwxQozFXkG6I
6A4TK1CJLXPvfz0UP0aBYyPmTNadDinaB9T4jIwd4rnxl+59GiEmqkN3IfPsv5Jj
MkKUmJnvOT0DEVlEaO1UZIwx5WpfprB3mR81/qm4XkAgmYrmgnLXd/pJDAMk7y1F
45b5zWofiD5l677lplcIPRbFhpJ6kDTODXh/XEdtF71EAeaOdEGOvyGDmCO0GWqS
FDkMMPTlieLA/0rgFTcz4xwUYj/cD5e0ZBuSkYsYFAU3hd1cGfBue0cPZaQH2HYx
Qk4zXD8S3F4690fRhr+tki5gyG6JDR67aKp3BIGLqm7f45WkX1hYp+YXywmEziM4
aSbGYhx8hoFGfq9UcfPEvp2aoc8u5sdqjDslhyUzM1v3m3ZGbhwEOnVjljY6JJLx
MxagxnZZSAY424ZZ3t71E/Mn27dm2w+xFRuoy8JEjv1d+BT3eChM5KaNwrj0IO/y
u8kFIgWYA1vZ/15qMT+tyJTfyrNVV/7Df7TNeWyNqjJ5rBmt0M6NpHG7CrUSkBy9
p8JhimgjP5r0FlEkgg+lyD+V79H98gQfVgP3pbJICz0SpBQf2F/2tyS4rLm+49rP
fcOajiXEuyhpcmzgusAj/1FjrtlynH1r9mnNaX4e+rLWzvU5AgMBAAGjUDBOMB0G
A1UdDgQWBBTkwyoJFGfYTVISTpM8E+igjdq28zAfBgNVHSMEGDAWgBTkwyoJFGfY
TVISTpM8E+igjdq28zAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4ICAQAR
OXnYwu3g1ZjHyley3fZI5aLPsaE17cOImVTehC8DcIphm2HOMR/hYTTL+V0G4P+u
gH+6xeRLKSHMHZTtSBIa6GDL03434y9CBuwGvAFCMU2GV8w92/Z7apkAhdLToZA/
X/iWP2jeaVJhxgEcH8uPrnSlqoPBcKC9PrgUzQYfSZJkLmB+3jEa3HKruy1abJP5
gAdQvwvcPpvYRnIzUc9fZODsVmlHVFBCl2dlu/iHh2h4GmL4Da2rRkUMlbVTdioB
UYIvMycdOkpH5wJftzw7cpjsudGas0PARDXCFfGyKhwBRFY7Xp7lbjtU5Rz0Gc04
lPrhDf0pFE98Aw4jJRpFeWMjpXUEaG1cq7D641RpgcMfPFvOHY47rvDTS7XJOaUT
BwRjmDt896s6vMDcaG/uXJbQjuzmmx3W2Idyh3s5SI0GTHb0IwMKYb4eBUIpQOnB
cE77VnCYqKvN1NVYAqhWjXbY7XasZvszCRcOG+W3FqNaHOK/n/0ueb0uijdLan+U
f4p1bjbAox8eAOQS/8a3bzkJzdyBNUKGx1BIK2IBL9bn/HravSDOiNRSnZ/R3l9G
ZauX0tu7IIDlRCILXSyeazu0aj/vdT3YFQXPcvt5Fkf5wiNTo53f72/jYEJd6qph
WrpoKqrwGwTpRUCMhYIUt65hsTxCiJJ5nKe39h46sg==
-----END CERTIFICATE-----',
        require => File['/home/denis/.gnupg'],
    }

    file { '/home/denis/.gnupg/gpg.conf':
        ensure  => file,
        owner   => 'denis',
        group   => 'denis',
        mode    => '0644',
        content => '
#-----------------------------
# algorithm and ciphers
#-----------------------------

personal-cipher-preferences AES256 AES192 AES CAST5
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
cert-digest-algo SHA512
s2k-digest-algo SHA512


#-----------------------------
# keyserver
#-----------------------------

auto-key-locate keyserver
keyserver hkps://hkps.pool.sks-keyservers.net
keyserver-options ca-cert-file=/home/denis/.gnupg/sks-keyservers.netCA.pem

# When using --refresh-keys, if the key in question has a preferred keyserver
# URL, then disable use of that preferred keyserver to refresh the key from
keyserver-options no-honor-keyserver-url

# When searching for a key with --search-keys, include keys that are marked on
# the keyserver as revoked
keyserver-options include-revoked


#-----------------------------
# behavior
#-----------------------------

# Display long key IDs
keyid-format 0xlong

# List all keys (or the specified ones) along with their fingerprints
with-fingerprint

# Disable inclusion of the version string in ASCII armored output
no-emit-version

# Disable comment string in clear text signatures and ASCII armored messages
no-comments

# Display the calculated validity of user IDs during key listings
list-options show-uid-validity
verify-options show-uid-validity

# Try to use the GnuPG-Agent. With this option, GnuPG first tries to connect to
# the agent before it asks for a passphrase.
use-agent
',
        require => File['/home/denis/.gnupg'],
    }

	file { '/home/denis/.gnupg/dirmngr.conf':
        ensure  => file,
        owner   => 'denis',
        group   => 'denis',
        mode    => '0644',
        content => 'hkp-cacert /home/denis/.gnupg/sks-keyservers.netCA.pem',
        require => File['/home/denis/.gnupg'],
    }

    file { '/home/denis/.gnupg/gpg-agent.conf':
        ensure  => file,
        owner   => 'denis',
        group   => 'denis',
        mode    => '0644',
        content => 'use-standard-socket
default-cache-ttl 43200
max-cache-ttl 43200
',
        require => File['/home/denis/.gnupg'],
    }

    # TODO: refresh gpg keys over Tor using "parcimonie" script
	file { '/etc/cron.d/denis-gpg-refresh-keys': # weekly refresh gpg keys
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => '12 3 * * 0 denis /usr/bin/gpg2 --refresh-keys',
    }
}
