class profile::base::ntp {
    $servers = hiera('profile::base::ntp::servers', [])
    $restrict = hiera('profile::base::ntp::restrict', [])

    class { '::ntp':
        servers   => $servers,
        restrict  => $restrict,
    }
}
