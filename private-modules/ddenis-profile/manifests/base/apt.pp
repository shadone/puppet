class profile::base::apt {
    include apt
    class { 'unattended_upgrades':
        auto => {
            'clean'                => 7,     # Remove packages that can no longer be downloaded from cache every X days
            'fix_interrupted_dpkg' => true,  # Try to fix package installation state.
            'reboot'               => false, # Reboot system after package update installation.
            'remove'               => true,  # Remove unneeded dependencies after update installation.
        },
        # mail => { 'to' => 'admin@domain.tld', }, # TODO: enable me when emailing works
    }
}
