class profile::logserver {
    package { 'rsyslog': ensure => installed }

    service { 'rsyslog':
        ensure  => running,
        enable  => true,
        require => File['/etc/rsyslog.conf'],
    }

    file { '/var/log/remote':
        ensure => 'directory',
        owner  => 'syslog',
        group  => 'adm',
        mode   => '0755',
    }
    file { '/var/log/remote/sw1':
        ensure => 'directory',
        owner  => 'syslog',
        group  => 'adm',
        mode   => '0755',
        require => File['/var/log/remote'],
    }
    file { '/var/log/remote/sw2':
        ensure => 'directory',
        owner  => 'syslog',
        group  => 'adm',
        mode   => '0755',
        require => File['/var/log/remote'],
    }
    file { '/var/log/remote/edge':
        ensure => 'directory',
        owner  => 'syslog',
        group  => 'adm',
        mode   => '0755',
        require => File['/var/log/remote'],
    }
    file { '/var/log/remote/vir':
        ensure => 'directory',
        owner  => 'syslog',
        group  => 'adm',
        mode   => '0755',
        require => File['/var/log/remote'],
    }
    file { '/var/log/remote/pve01':
        ensure => 'directory',
        owner  => 'syslog',
        group  => 'adm',
        mode   => '0755',
        require => File['/var/log/remote'],
    }
    file { '/var/log/remote/pve02':
        ensure => 'directory',
        owner  => 'syslog',
        group  => 'adm',
        mode   => '0755',
        require => File['/var/log/remote'],
    }
    file { '/var/log/remote/unknown':
        ensure => 'directory',
        owner  => 'syslog',
        group  => 'adm',
        mode   => '0755',
        require => File['/var/log/remote'],
    }

    file { '/etc/rsyslog.conf':
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet:///modules/profile/logserver/rsyslog.conf',
        require => [
            File['/var/log/remote/sw1'],
            File['/var/log/remote/sw2'],
            File['/var/log/remote/edge'],
            File['/var/log/remote/vir'],
            File['/var/log/remote/pve01'],
            File['/var/log/remote/pve02'],
            File['/var/log/remote/unknown'],
            Package['rsyslog'],
        ],
        notify  => Service['rsyslog'],
    }

    package { 'logrotate': ensure => installed }
    file { '/etc/logrotate.d/rsyslog-remotelogs':
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet:///modules/profile/logserver/logrotate-remotelogs.conf',
        require => Package['logrotate'],
    }
}
