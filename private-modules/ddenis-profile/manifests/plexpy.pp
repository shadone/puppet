class profile::plexpy {
    $version = hiera('profile::plexpy::version')
    $host = hiera('profile::plexpy::host', '0.0.0.0')
    $port = hiera('profile::plexpy::port', 80)

    include 'docker'

    file { '/opt/plexpy':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }
    file { '/opt/plexpy/config':
        ensure => 'directory',
        mode   => '0755',
        require => File['/opt/plexpy'],
    }
    file { '/opt/plexpy/data':
        ensure => 'directory',
        mode   => '0755',
        require => File['/opt/plexpy'],
    }

    docker::image { 'ddenis/plexpy':
        ensure    => present,
        image_tag => $version,
    }
    docker::run { 'plexpy':
        image   => "ddenis/plexpy:${version}",
        command => 'plexpy',
        ports   => ["${host}:${port}:8080"],
        volumes => [
            '/opt/plexpy/config:/config',
            '/opt/plexpy/data:/data',
        ],
        require => [
            File['/opt/plexpy/config'],
            File['/opt/plexpy/data'],
        ],
    }
}
