class profile::plexmediaserver {
    $uid = hiera('profile::plexmediaserver::uid', undef)
    $version = hiera('profile::plexmediaserver::version')
    $host = hiera('profile::plexmediaserver::host', '0.0.0.0')
    $port = hiera('profile::plexmediaserver::port', 32400)

    if $uid == undef or (is_string($uid) and empty($uid)) {
        $uid_env_var = []
    } elsif is_integer($uid) {
        $uid_env_var = [ "PLEX_UID=${uid}" ]
    } else {
        fail('uid should be a number.')
    }

    class { 'fstab': }

    fstab::mount { '/mnt/media':
        ensure  => 'mounted',
        device  => '10.68.64.10:/volume1/media',
        options => 'ro,async,intr',
        fstype  => 'nfs',
    }

    include 'docker'

    file { '/opt/plex':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }
    file { '/opt/plex/config':
        ensure => 'directory',
        mode   => '0755',
        require => File['/opt/plex'],
    }

    docker::image { 'ddenis/plexmediaserver':
        ensure    => present,
        image_tag => $version,
    }
    docker::run { 'plexmediaserver':
        image   => "ddenis/plexmediaserver:${version}",
        command => 'plex',
        ports   => ["${host}:${port}:32400"],
        volumes => [
            '/opt/plex/config:/config',
            '/mnt/media:/data:ro',
        ],
        env     => concat([
        ], $uid_env_var),
        require => [
            File['/opt/plex/config'],
            Fstab::Mount['/mnt/media'],
        ],
    }
}
