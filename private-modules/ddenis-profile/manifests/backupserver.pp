class profile::backupserver {
    class {'fstab': }
    class {'docker': }
    class {'profile::backupserver::annex': }
    class {'profile::backupserver::coldstorage': }
    class {'profile::backupserver::gmvault': }
}